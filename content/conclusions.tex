\chapter{Conclusions}
\label{chap:conclusions}

This report discussed a project which aims to extend the existing incomplete Modus prototype and perform evaluation of Modus on building real-world applications. Both of these goals have been achieved and details documented in this report. Specifically, this project implemented new proposed language constructs such as builtin predicates and operators, a set of builtins which enables Modus to express complex container build logic, a new container-building backend based on BuildKit which supports parallelization and caching, and improved existing learning material and documentation for Modus. In \Cref{chap:evaluation}, Modus was also evaluated on 6 popular open-source projects, supporting the claim that Modus addresses the limitations of conventional Dockerfiles and enables developers to write more concise container builds.

\section{Outcomes}

The main deliverable of this project includes this report as well as the Modus source code, for which this project was a major contributor. Aside from these two, works presented here also contributed to a submitted research paper on Modus currently under review, as well as the Modus website \url{https://modus-continens.com/}, documentation \url{https://docs.modus-continens.com/}, and other related resources. By the end of this project, Modus has improved substantially in terms of features and ease-of-use, and was made available to the public.

\begin{framed}
  \noindent\centering\begin{minipage}{26.7em}
    \noindent All source code for the Modus project can be found at:

    \centerline{\url{https://github.com/modus-continens/modus}}

    \noindent At the time of writing, the latest commit is
    \href{https://github.com/modus-continens/modus/tree/9c06c5bc3b35c92c03ab89a129cf6eddcab92a88}{%
      \texttt{9c06c5b} {\color{link}(click to browse)}
    }\vspace{-\baselineskip}%
    %
    % \noindent Backup download in case the above link is broken:
    %
    % \centerline{{\small\url{https://fileshare.maowtm.org/fdf9304e41f7b3c09434-modus-main.zip}}}
  \end{minipage}
\end{framed}

While a lot more could be improved, our evaluation showed positive result, indicating that Modus is a promising solution that addresses some of the pain points of Dockerfiles.

\section{Limitations}

At the time of writing, Modus is still in an early, experimental stage, and not all problems are addressed in this project. This section lists out some of the limitations of Modus that can be worked on further.

\textbf{Overheads from BuildKit workarounds}:
In \Cref{sec:handling-multiple-output-images}, we mentioned that Modus currently uses a workaround to output multiple images, due to the limitation of BuildKit API. Setting aside the inelegant design, this workaround also introduces around 0.5 to \SI{1}{s} of extra build time at the end for each output image. This could be addressed ideally by an API change upstream, but Modus could also potentially find a more low-level API to invoke BuildKit to alleviate this overhead.

\textbf{Error messages}:
Error printed by Modus includes syntax error, resolution error (such as undefined predicates, unification failure or insufficient groundness error) or type-check errors (such as having multiple image predicates in one rule). These errors can sometime be hard to understand, as they do not necessarily reflect the most likely root of the problem, causing user frustration and potentially affect user acceptance. This could be improved by providing more relevant hints, possibly through pattern-matching against common mistakes.

\textbf{Windows and Mac binaries}:
As mentioned in \Cref{sec:impl-binary-dist}, Modus currently publishes binaries for x86\_64 and ARM64 Linux. The CI workflow should be expanded to compile binaries for other platforms as well, to ease the installation process on those, either by using a CI running on MacOS and Windows, or by using cross-compiling toolchains for the respective platforms.

\textbf{Windows containers}:
Modus currently only supports Linux containers, even though Docker itself has added support for containers based on Windows. Some projects on Docker Hub, such as OpenJDK~\cite{openjdkDockerURL}, have already adopted Windows containers. In order for Modus to be a full replacement for Dockerfiles for these projects, Modus need to support Windows containers.

\textbf{Defining custom operators}:
While Modus does have operators, there is currently no way for a user to define them. In our evaluation this has occasionally caused some code duplication, which could be avoided if Modus allowed users to easily define custom operators. Ideally, a new syntax should be considered for operator definition.

Aside from these, the adoption of Modus can also be limited by fact that Modus is based on a logic programming language that is quite specialized, and most developers in general are not likely to be familiar with it. This could prove to be a significant barrier to the understanding and use of Modus by end users, and make it harder to justify the use of Modus. Future works may be able to address this limitation by creating more user-friendly error messages, as well as providing more introductory documentation and examples.

\section{Future Works}
\label{sec:future-works}

In addition to addressing some of the limitations listed above, there are a number of areas that can be worked on further in Modus.

It has been proposed for Modus to support more constant types other than strings. For example, the ability to use and manipulate arrays can prove useful in modelling \eg a list of packages to install. This potentially means that Modus would need a more powerful type system that would allow defining and using predicates which takes arrays or other types. More logical predicates can be implemented on top of the new type system, such as the ability to handle structured data in the form of JSON or CSV files.

Another future direction of the Modus project is the creation of a ``Modus registry'', similar to Docker Hub. This registry could allow developers to easily publish or download images built by Modus, by referring to them as Modus literals. For example, instead of \lstinline{node:18-alpine}, a NodeJS image could be referred to as \lstinline{node("18", "alpine")}, and these predicates can be directly used in Modusfiles.

Since Modusfiles and Dockerfiles represent very similar things fundamentally, it is possible to translate between them. An automatic transpiler from Dockerfile to Modusfile could help existing projects adopt Modus quicker and reduce mistakes.

Finally, Modus could benefit from more rigorous testing. Beside those discussed in \Cref{chap:testing}, there are also other ways to enhance the testing of Modus, such as by using fuzz testing~\cite{libfuzzer} to ensure that Modus does not crash under any input.

% \textbf{Build Graph Correctness}:
% Since Modus essentially outputs a build graph, checking that the output from a test case is equal to some previously known correct output can be a good way to detect unintended changes. This can be done by checking the final binary-representation of the graph sent to BuildKit, although it may require slight modifications to the library. One can also check the Modus internal representation (see \Cref{sec:impl-buildkit-backend})

\section{Final Thoughts}

This project involved considerable amount of engineering work, and produced a code repository containing multiple thousand-line files. Overall, the project produced positive results, although some of the implementation approaches can be improved in hindsight. As discussed earlier, missing pieces like more complex types and custom operators can be added, and there are also several limitations that are not addressed. With that said, as shown by the evaluation, Modus already solves problems in real world container infrastructure. The existing work done on Modus also serve as a foundation for future extensions to support more use cases.

A number of other applications of Modus have also been discussed from time to time during this project. For example, Modus could be used to improve the ease-of-use and reproducibility of some of the existing tools in the field of program repair research, given that projects like BugZoo~\cite{timperley2018bugzoo} have explored the use of containers in this area. There are also potential to apply Modus in the broader domain of reproducible software builds and reproducible research, which often takes advantages of containerization~\cite{docker-for-reproducible-re}.

As a final note, I would like to thank my supervisor for his guidance and support throughout the project, as well as \redacted{Chris Tomy} and his supervisor \redacted{Earl Barr}, who has been working together on another final year project that also contributes to Modus, specifically focusing on logic resolution and other front-end matters. This project would not have been possible without the work from both of them.
