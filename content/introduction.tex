\chapter{Introduction}
\label{chap:introduction}

\emph{Modus} is a language that applies logic programming to building container image. It was recently proposed to address core limitations of existing container building infrastructure: inability to define complex build workflows, slow builds and bloated images. At the beginning of this project, Modus only existed as an incomplete prototype. It lacked critical features, which limited its potential in efficiency and expressiveness, and its logical system was proved to be intractable. It also did not have any user-friendly documentation and examples, and the practical impact of Modus was not formally evaluated. The core goal of this project is to address these challenges by extending Modus and evaluating it on a set of existing open-source projects on Docker Hub.

At the time of writing, Modus has been released to the public:

\vskip20pt plus 10pt\relax

\definecolor{modus-bg}{HTML}{8ec5fc}%
\noindent\centerline{\href{https://modus-continens.com/}{%
  \begin{tikzpicture}
    \node[inner sep=1em] (logo) {\includegraphics[width=4em]{logo.pdf}};
    \node[right=0pt of logo, inner ysep=10pt, text width=25em, align=left] (text) {%
      \sffamily%
      \Large \textbf{Modus} \raisebox{-0.5pt}{\href{https://crates.io/crates/modus}{\includegraphics[height=10pt]{badges.pdf}}} \\
      \normalsize A language for building Docker/OCI container images \\
      \small \href{https://modus-continens.com}{modus-continens.com} $\cdot$ \href{https://github.com/modus-continens/modus}{github.com/modus-continens/modus}
    };
    \begin{scope}[on background layer]
      \node[fill=modus-bg, fit=(logo)(text), fill opacity=0.145] {};
    \end{scope}
  \end{tikzpicture}
}}

\vskip10pt plus 10pt\relax

This project contributed extensively to the implementation of various features within Modus, and created a comprehensive set of unit and integration tests, documentations and tutorials for the new language, as well as a streamlined binary release workflow in order to simplify installation. A \href{https://play.modus-continens.com/}{\color{link}Modus Playground}%
\footnote{\url{https://play.modus-continens.com/}}
was also created as part of this project in order to better demonstrate how Modus works to new users.

This chapter will start by briefly introducing the existing container landscape, summarizing what its limitations are, and what the current state of Modus is before this project. It will then move on to discuss what the project intends to achieve by the end. The final section will conclude this chapter with brief summaries of each of the following chapters in this report.

\section{Background}
\label{sec:background}

\emph{Containers} are packaged distribution of one or more applications along with all of its dependencies~\cite{what-is-a-container}, in the form of a self-contained root filesystem. Containers simplify deployment of software and enable per-application isolation, and as such they are a widely adopted approach of packaging applications in the industry, supported by most popular open-source server software including NodeJS, Redis, OpenJDK, MySQL, Golang, \etcetra. According to a popular developer survey, Docker~\cite{merkel2014docker} is the most popular container engine~\cite{stack-overflow-survey-tools}, with the ability to build, run and manage containers with its command line tool and a special language known as \emph{Dockerfile}. Docker is maintained by Docker Inc., who also manages a registry of Docker images known as \emph{Docker Hub}~\cite{docker-hub}.

At a high level, Dockerfiles are instructions that Docker follows to build a container image. Common commands include \texttt{FROM} (to base the build on another existing image), \texttt{RUN} (to execute a shell script), \texttt{COPY} (to copy files into the image) and commands that sets certain properties of the image, such as entrypoint or working directory~\cite{dockerfile-reference}.

In Docker, container images use the OCI image format~\cite{oci-image-spec}. Notably, an OCI image contains a number of \emph{layers}, which are merged together at runtime via a union-mount filesystem. This has various implications over how images are built---more layers mean a more granular build cache (potentially shortening build time), but can also make the image larger. Therefore, it is common for developers to want control over how their images are layered. In Dockerfile, most build instructions, including \lstinline{RUN} and \lstinline{COPY}, generate a new layer every time they are used.

While Docker has been very successful in a wide range of use-cases and in gaining adoption by popular applications, Dockerfiles have important limitations. They are unable to express interactions among parameters or to specify complex build workflows that build multiple images, are difficult and inefficient to parallelise and cache, tent to produce bloated images, and are costly to maintain. Dockerfile's lack of expressiveness forced many popular projects to generate Dockerfile from templates using \adhoc scripts, as we will explore in \Cref{sec:porting-to-modus}. \Cref{sec:limitations-of-dockerfile} contains a more in-depth discussion of these problems.

Datalog~\cite{ceri1989you} is a programming language implementing a decidable subset of predicate logic. A Datalog program is made up of \emph{predicates} like \lstinline{p(x, y)} and \emph{rules} that denote how predicates can be proven. For example, \texttt{p(x, y) :- q(x), r(y)} means that \lstinline{p(x, y)} is true if \lstinline{q(x)} and \lstinline{r(y)} are true. Implementations of Datalog typically allows users to specify a goal to be proven, and will output a proof starting from predicates known to be true, using the rules in the program to reach the goal. Datalog dialects often implement domain-specific extensions such as builtin predicates with their own semantics, allowing programs to express special logic or relevant operations.

Modus is a recently proposed dialect of Datalog for building container images, an alternative to Dockerfiles. It addresses the problems of Dockerfile by (1) using Datalog to express more complex logic that can interact with parameters, removing the need for templating or duplicated Dockerfile instructions; (2) using a backend that supports any number of output images while still caching shared layers; and (3) adding features like ``merge'' to allow fine-grained control of image layers.

Modus implementation consists of two parts: a frontend that parses and solves Datalog rules, and a backend that execute build steps resolved during the solving stage. At the beginning of this project, Modus existed only as an incomplete prototype\footnote{Initial prototype of Modus: \\ \url{https://github.com/modus-continens/modus/tree/initial-prototype}}, which lacked major features:

\begin{enumerate}
  \item Modus' frontend implemented only a subset of its syntax.\label{enum:limitation-builtins}
  \item Modus' backend worked by transpiling into Dockerfile, which made parallelisation inefficient and  limited the ability to optimise image size.
  \item It lacked user-accessible documentation, examples, and tutorial.
\end{enumerate}

Several major features of Modus only existed in the form of proposals, and details around how to implement them and potential issues were not yet determined. These features include builtin predicates, scoped operators and various other ease-of-use improvements. \Cref{sec:context-modus} outlines, in more detail, the state of Modus at the beginning of the project, as well as any proposed language features. Aside from missing features, Modus' claims were also not evaluated, as it has not been applied to any real-world projects.

\section{Intended Outcome}
\label{sec:intended-outcome}

The aims of this project falls into two distinct groups: implementing core Modus features, and evaluating Modus on realistic use cases.

\subsection{Modus Implementation}
\label{sec:intro-modus-impl-outcome}

One of the goals of this project is to turn Modus from an early prototype into a release-ready tool for building Docker containers. This means that it has to be expressive, easy-to-use and efficient, and there should be enough documentation and examples to help new users.

Specifically, this project will first implement support for more language constructs for building images and expressing logic. These include building blocks like builtin predicates and operators: builtin predicates can be directly used to encode build instructions or express logical statements commonly used in building software that are otherwise not expressible in Datalog, and operators are predicates that can be attached to a set of other predicates and change their behaviour. Operators are necessary for multi-stage builds and can be used to support various other features. A set of builtin predicates and operators has been proposed, and this project will also implement them.

A backend will also be implemented that addresses the limitations of current implementation. It will support efficient parallelisation in the case of multiple output images, as well as other proposed features such as the ``merge'' operator which allow fine-grained control of image layers.

To ensure correctness, we will also create a comprehensive set of unit and integration tests (also known as ``system tests'') for Modus, of which there are currently none.

Finally, while there is some existing documentation for Modus, they are incomplete and not aimed at end users, with the focus mostly on design decisions. As such, this project aims to improve the documentation of Modus, create a tutorial for beginners, and create more examples, making it easier for new users to get started. To reduce friction, we will also improve the installation process by creating binary distributions, and creating a playground for users to quickly experiment with the language.

\subsection{Modus Evaluation}

Another core goal of this project is to evaluate the improved Modus by applying it to building containers for existing applications. Specifically, Modus will be used to build a more concise and efficient replacement for the existing build system used by some popular projects on Docker Hub. As we will see in \Cref{chap:evaluation}, many of these projects use templating scripts to generate their Dockerfiles, often involving \adhoc string processing tools like \lstinline{jq} and \lstinline{awk}, and can be hard to understand and maintain. The evaluation results will also contribute to paper submission presenting Modus.

\section{Scope}
\label{sec:scope}

Modus is a newly proposed build system for container images, and is especially useful for projects which maintains multiple related images or have complex build requirements.

This project will work on implementing various Modus functionalities, including those listed in \Cref{sec:intro-modus-impl-outcome}. The focus is mostly on the backend, starting from transforming a solved proof into BuildKit instructions. This project will also help the adoption of Modus by contributing to the documentations, website and other relevant items, and will also evaluate Modus by investigating existing approaches to container building, and create Modus-based replacements for some open-source projects.

During the time of this project, Modus was also worked on by another student at UCL---\redacted{Chris Tomy}---as part of his final year project. Although we will work closely together as we are both contributing to Modus, we will be focusing on separate topics. For example, \redacted{Chris} will be implementing a parser and type-checker for the new language, a proof solver for our Datalog-based logic, and he will also evaluate Modus on OpenJDK. These areas are therefore not part of this project.

With that said, in the early part of my project I have done some background research regarding the Turing-completeness of unrestricted Datalog with certain proposed builtin predicates, and therefore this work is presented in this report. I have also commented on areas worked on by \redacted{Chris} from time to time, including suggestions on aspects of the implementation, and I have also contributed to some pre-processing implementation in the frontend.

\section{Content Contributions Disclaimer}

During this project I have contributed to a submitted paper on Modus, authored by \redacted{Chris Tomy}, me, \redacted{Earl Barr}, and \redacted{Sergey Mechtaev}. Due to overlap in area, \cref{chap:context} and \ref{chap:evaluation} of this report contains extract from respective sections in the paper, which contains contributions from all 4 of us. Permission has been granted by the respective authors in the case where content has been reproduced. Regardless, substantial new content has been added to \cref{chap:context}, and \cref{chap:evaluation} are predominantly authored by me in the first place.

\section{Chapters Overview}
\label{sec:chapters-overview}

The rest of this report is structured into 6 chapters. \textbf{\Cref{chap:context}} contains more information about containers, Docker, the limitation of Dockerfiles, BuildKit, and discusses the Modus proposal in detail. \textbf{\Cref{chap:problems}} discusses problems and potential challenges that this project has to address. \textbf{\Cref{chap:solution-and-impl}} follows with our solution and implementation to these problems. \textbf{\Cref{chap:testing}} is a short overview of our correctness testing approach, including discussion about unit and system tests, and continuous integration. \textbf{\Cref{chap:evaluation}} discusses the evaluation of Modus on real-world projects, and \textbf{\Cref{chap:conclusions}} summarizes the achievements of this project and any potential future works.

This report also comes with an appendix, which contains the initial project plan and interim report submitted to the department, as well as a listing of some of the source code produced in this project. Interested readers are recommended to explore the source code online, in the \href{https://github.com/modus-continens/modus}{\color{link}Modus GitHub repository}.
