\chapter{Problems \& Analysis}
\label{chap:problems}

Various features have been proposed for Modus, but not implemented, before the start of this project. They range from new syntax, builtin predicates and operators to command line (CLI) features that improve ease-of-use. There is also the need for a better backend which supports parallelisation, more fine-grained cache control and other features absent from Dockerfiles that would be required to implement some of the proposed features, such as merge. Each section in this chapter focuses on one area, outlining any problems and any potential related issues in detail, and discuss why those problems need to be addressed. \Cref{chap:solution-and-impl}, which follows this chapter, discusses our solutions and implementations.

\section{Builtin Predicates}

Builtin predicates are predicates that are defined by Modus itself, and can be directly used by users of the language. There are roughly two distinct categories of builtin predicates, and they are necessary for separate reasons.

The first class of builtin predicates are container building instructions. While Modus provides the ability to write predicate logic rules that compactly captures dependencies and interaction with parameters, this is only part of the story. In order to build a container image in the way proposed in our new Datalog-based language (\Cref{sec:using-datalog-to-docker}), there needs to be a way to interacts with the outside environment and manipulate container images. This is done via the introduction of builtin predicates which encodes build instructions. These include predicates like \lstinline{from}, which sets the base image to be built upon, \lstinline{run}, which adds a layer by running a command, and \lstinline{copy}, which copies files into the current image.

The second class of builtin predicates needed are those that encode logic likely to be useful in the building of container images, but are not expressible in pure Datalog. Various builtin predicates has been proposed in this respect, these include:

\begin{itemize}
  \item String manipulation: \lstinline{string_concat}, regex, \etc

  \item Parsing of common string representations. These would come in the form of \textit{format\_name}\lstinline[mathescape]{(full_str, part_1, part_2, $\ldots$)}. Examples include:

  \begin{itemize}
    \item \lstinline{docker_image(ref, registry, repo, tag)}
    \item \lstinline{url(url, proto, auth, host, port, path)}, \etc
  \end{itemize}

  \item Number and Semantic Version~\cite{semverURL} comparisons: \lstinline{number_gt}, \lstinline{semver_leq}, \etc
\end{itemize}

Because they implement logic not expressible in Datalog, they can not be implemented with the Modus language itself. Instead, the logic for these predicates must be implemented in native code, and the proof solver need to be modified to support them by invoking the relevant functions when these predicates are encountered.

% Aside from this, two other issues need to be considered when implementing builtin predicates: they need to handle ungrounded variables, and that introducing arbitrary builtin predicates can affect the decidability of Modus programs.

Moreover, implementations of builtin predicates may need to do more than just returning true or false: Certain builtins have a ``bidirectional'' semantics where they can accept ungrounded variables as arguments and place constraints on their final values. For example, \lstinline{string_concat(A, B, C)} is a predicate which denotes the constraint that $\texttt{C} = \texttt{A} \concat \texttt{B}$, where $\concat$ is the string concatenation operation. Stating \lstinline{string_concat("a", "b", X)} should be equivalent to directly stating \lstinline{X = "ab".}. Careful consideration is also required to handle cases where there is not enough information yet to reduce a builtin predicate to a finite set of concrete equality constraints, such as \lstinline{string_concat(A, "b", C)}.

Finally, standard Datalog is decidable, because it operates over finite relations. In contrast, builtin predicates induce a potentially infinite set of ground facts. For example, aside from \lstinline{string_concat}, the relation \lstinline{number_lt(X, Y)} that checks if the number \lstinline{X} is less than the number \lstinline{Y} is also an infinite relation, because there are an infinite number of true facts, e.g. \lstinline{number_lt(1, 2)}, \lstinline{number_lt(1, 3)}, \lstinline{number_lt(2, 3)}, \etcetra. Using such infinite relations in a Datalog program means that logical resolution may no longer be tractable, \ie that Modus may not terminate~\cite[Chapter 11]{ceri2012logic}. Thus, builtin predicates require a special treatment.

\begin{listing}[p]
  \centering%
  \begin{minipage}{17em}
    \begin{minted}{prolog}
      add("", "", "").
      add("", X, X) :- X = X.
      add(X, "", X) :- X = X.

      add(X, Y, R) :-
        string_concat(Xrest, "0", X),
        string_concat(Yrest, "0", Y),
        string_concat(Rrest, "0", R),
        add(Xrest, Yrest, Rrest).

      add(X, Y, R) :-
        string_concat(Xrest, "1", X),
        string_concat(Yrest, "0", Y),
        string_concat(Rrest, "1", R),
        add(Xrest, Yrest, Rrest).

      add(X, Y, R) :-
        string_concat(Xrest, "0", X),
        string_concat(Yrest, "1", Y),
        string_concat(Rrest, "1", R),
        add(Xrest, Yrest, Rrest).

      add(X, Y, R) :-
        string_concat(Xrest, "1", X),
        string_concat(Yrest, "1", Y),
        string_concat(Rrestinc, "0", R),
        add(Xrest, Yrest, Rrest),
        inc(Rrest, Rrestinc).

      inc("", "1").
      inc("0", "1").
      inc(X, R) :-
        string_concat(Xrest, "0", X),
        R = f"${Xrest}1".

      inc(X, R) :-
        string_concat(Xrest, "1", X),
        string_concat(Xrestinc, "0", R),
        inc(Xrest, Xrestinc).
    \end{minted}
  \end{minipage}~
  \begin{minipage}{14em}
    {\footnotesize%
      \dirtree{%
        .1 add("11", "1101", "10000").
        .2 add("1", "110", "111").
        .3 add("", "11", "11").
        .2 inc("111", "1000").
        .3 inc("11", "100").
        .4 inc("1", "10").
        .5 inc("", "1").
      }
    }
  \end{minipage}

  \caption{Binary addition implemented with \lstinline{string_concat} (left). Proof tree for \lstinline{add("11", "1101", R)} (right).}
  \label{lst:binary-addition}
\end{listing}

To more firmly illustrate the last point regarding tractability, let us consider the example in \Cref{lst:binary-addition}, which implements a binary addition predicate with only the \lstinline{string_concat} builtin. We also constructed similar program for parsing the context-sensitive grammar $\texttt{a}^n \texttt{b}^n \texttt{c}^n$. While these examples do not on their own proves Turning completeness of Datalog with unrestricted \lstinline{string_concat}, they are strong evidence for the power of \lstinline{string_concat}. In principle, because arbitrarily long strings can be constructed recursively, and that when limited to a finite alphabet, predicates can select and replace the first character with \lstinline{string_concat}, it is possible to implement any Turing machine in this setup. \cite{bonner1998sequences}

While the decidability problem with \lstinline{string_concat} is discussed here, resolving it requires a stratification-based approach, and is out of the scope of this project. This stratification will be implemented as a restriction on recursions involving \lstinline{string_concat} in the frontend.

\section{Operators}

Builtin predicates alone are not able to express all container building needs. For example, multi-stage builds require the ability to copy files from other intermediate images. However, there is currently no way for \lstinline{copy} to refer to another image in the same Modusfile as the source. To address this problem, a special kind of predicate known as \emph{operators} has been proposed, which can be applied to a sub-expression containing other literals, as a form of label for the sub-expression.

In the Modus language, they are written in the form \lstinline[mathescape]{expr::operator(v$_1$, $\ldots$, v$_n$)}, where \texttt{expr} can be another literal, a chain of literals like \lstinline[mathescape]{(p$_1$($...$), p$_2$($...$), $\ldots$)}, or any other forms of logical expression. The operator, along with the expression it applies to, are considered a new valid expression, which allows operators to be nested. The operator itself and its arguments (\lstinline[mathescape]{operator(v$_1$, $\ldots$, v$_n$)}) is considered a literal, and they should participate in proof resolution just like any other predicates.

For some concrete examples, a number of operators have been proposed for Modus. \lstinline[mathescape]{$I$::copy(src, dest)} copies the file or directory \texttt{src} from another image $I$ into \texttt{dest} in the current image. $I$ can be a literal for another image, or be a nested image expression like \lstinline[mathescape]{(from($...$), run($...$), ...)}. A set of operators such as \lstinline{::set_env}, \lstinline{::set_workdir}, \lstinline{::set_cmd}, \etcetra are proposed to allow users to alter OCI image properties, like \lstinline{ENV}, \lstinline{WORKDIR} and \lstinline{CMD} in Dockerfile. They take the form \lstinline[mathescape]{$I$::set_}\textit{name}\lstinline[mathescape]{($...$)}, where $I$ is a source image, and the entire expression represents the image $I$ with the desired property change. \lstinline[mathescape]{$L$::in_env(env)} and \lstinline[mathescape]{$L$::in_workdir(path)} are also proposed, which temporarily changes the environment variables and working directory for specific layers.

A newly proposed operator which is particularly interesting is \lstinline{::merge}, which implements a special build function not available in Dockerfiles. When building container images, it is often desirable for multiple operations to be done in a single image layer, as new layers can not delete cache or other unused files from older layers, causing a bigger image size. The \lstinline{merge} operator can be used to squash a set of \lstinline{run}, \lstinline{copy} and \lstinline{::copy} build instructions into one image layer. This means that, unlike Dockerfiles, users of Modus will not have to put everything inside one build instruction in order to achieve the same effect.

\Cref{lst:example-operator-usage} shows an example program making use of the proposed operators. The example first installs apt dependencies and clears apt repository cache in the same layer with \lstinline{merge}, using \lstinline{set_env} to temporarily set an environment variable that would not persist in the later stages or the final image, then builds two components of the application in different subdirectories, using \lstinline{in_workdir} to temporarily change the current directory.

\begin{listing}
  \centering\begin{minipage}{22.5em}
    \begin{minted}{prolog}
      app :- (
        from("debian")::set_workdir("/usr/src/app"),
        (
          run("apt update"),
          run("apt install -y ..."),
          run("rm -rf /var/lib/apt/lists/*")
        )::in_env("DEBIAN_FRONTEND", "noninteractive")
         ::merge,
        (
          run("git clone ..."),
          run("make")
        )::in_workdir("native"),
        (
          copy(".", "."),
          run("npm i")
        )::in_workdir("node")
      )::set_entrypoint("./node/main.js").
    \end{minted}
  \end{minipage}

  \caption{Example usage of operators.}
  \label{lst:example-operator-usage}
\end{listing}

Aside from the need to implement the proposed operators, there is another problem that need to be addressed. Operators, as proposed here, does not map nicely to Datalog. In both Datalog and general first-order logic, predicates can not take other predicate or logical statements as input. Nevertheless, operators can not be separated from the proof resolution process altogether, as it is desirable to have them behave like other predicates that can take variable as arguments. This would be useful for cases where the file to copy depends on parameters, among other possibilities. In the most general case, operators can even impose constraints on arguments just like normal predicates. Even though this is not the case for any of our currently proposed builtin operators, an implementation that is flexible enough to support such use cases in the future is desired. Therefore, we need a useful way to encode operators in terms of existing Datalog concepts.

\section{A New BuildKit-based Backend}
\label{sec:problem-buildkit-backend}

Given that the newly proposed language encodes build instructions in a way radically different from the initial prototype (see \Cref{sec:existing-prototype-impl}), a significantly reworked backend is likely required. Moreover, multi-stage builds and the ability to set image properties need to be implemented as they are not present in the prototype backend, as well as support for the new \lstinline{::merge} operator.

Aside from necessary changes and missing features, the existing prototype backend was based on transpiling into Dockerfile. This approach has some fundamental limitations: multiple outputs and parallel builds are not supported, and the fact that Dockerfile is a complex language to serialize into, potentially leading to string escape related bugs.

In \Cref{sec:buildkit} we explored a new, promising container building framework known as BuildKit. It takes as input a directed acyclic graph (DAG) of build instructions, with edges representing dependencies (such as parent image or source image to copy files from). In Modus, build instructions are represented by a chain of predicates and operators, and operators like \lstinline{::copy} reference other images as their input. This means that Modusfile maps naturally to such DAG. Moreover, BuildKit supports all build instructions required by Modus, handles parallelisation and caching, and is on course to become the default backend for \lstinline{docker build}, currently supported by Docker behind an environment variable flag. Considering all of the above, BuildKit is likely to be the best available option for implementing the new Modus backend.

With that said, there is still a challenge in using BuildKit for Modus: While BuildKit does support building multiple intermediate stages in parallel automatically, it does not support multiple output images in one build graph, as the API requires that one particular node be marked as the output. Since a major feature of Modus is the ability to generate multiple images in one query, a workaround will be required to address this issue.

\section{Docker Integration}
\label{sec:problem-docker-integration}

The previous \the\numexpr\arabic{section} - 1\relax{} sections of this chapter focused on core issues which enable Modus to work. For the remainder of this chapter, we will turn our attention to other miscellaneous areas for improvement, including tighter integration with Docker, the command line interface, installation and documentation.

In Modus, images can be identified with literals such as \lstinline{app("alpine", "dev", "release")} (reusing the example in \Cref{lst:example-modusfile}). However, after they are built, these identifiers can not be used with Docker to run the image and perform other operations. User of Modus images need to refer to them via their hash or tag them with Docker, just like any ordinary Docker images, which is not convenient. Moreover, Docker image tags do not map conveniently to Modus literals---tags in Docker can only contain alphanumeric characters, underscores, and dashes, and multiple parameters must be concatenated together.

A proposed feature that would make using and managing Modus images easier is to support a subset of Docker commands in Modus, with the alteration that Modus literals can be used to refer to images. For example, the following commands would build Alpine dev and prod images for \lstinline{app} and run \lstinline{app("alpine", "prod", "release")}:

\begin{minted}{bash}
  modus build . 'app("alpine", X, "release")'
  modus run [docker-run flags...] 'app("alpine", "prod", "release")'
\end{minted}

A command that lists all available images matching a literal could be useful too:

{
\vskip\topsep%
\parskip=0pt%
\partopsep=-\topsep%
\topsep=0pt%
\begin{minted}[linenos=false]{bash}
  $ modus ls 'app(a, b, c)'
\end{minted}
\begin{minted}[linenos=false]{text}
  app("alpine", "dev", "release")
  app("alpine", "prod", "release")
  ...
\end{minted}
}
% \vskip\topsep%

\section{Installation}

At the start of the project Modus does not use any distribution channel other than the Rust repository \lstinline{crates.io}. Users that already have Rust installed can install Modus with \lstinline{cargo install modus}. However, this is inconvenient for various reasons: it requires Rust to be installed first, even though Modus does not depend on Rust to run; it compiles Modus and any library dependencies from source, which can take several minutes on machines without top-tier CPUs; compilation can also fail if the user has an out-of-date version of Rust installed.

To tackle these problems, most commonly-used open-source projects provide binary distributions for their supported platforms. Installation often only requires copying the pre-compiled binaries to a suitable place in the user's file system. As such, Modus should also provide binary distributions for each release, which can be built automatically on a CI platform like GitHub Actions.

\section{More Documentation \& Interactive Playground}
\label{sec:more-doc-and-playground}

While Modus does have some documentation at the beginning of this project, they are incomplete and not geared at new users. This project will expand on the existing documentation by adding more examples and documenting new features and builtins.

Existing documentation for Modus exists in the form of a collection of Markdown files tied together mdbook~\cite{mdbook}. This project will therefore extend upon those. The latest documentation can be found at \url{https://docs.modus-continens.com/}.

To make it easier to introduce Modus, we will also create an online interactive playground for Modus, which can be opened in a web browser and used to experiment with the language, without installing Modus locally. The playground should show the solved proof tree corresponding to the user's query, possibly in the format as shown in \Cref{fig:app-example-proof:tree} (on page~\pageref{fig:app-example-proof:tree}). There are two ways this can be implemented---running Modus on a server, or building a WebAssembly (WASM) version of Modus to run in the browser.

Since a server comes with cost implications and the need for maintenance, the WASM approach was explored first. The main challenge is to separate the logic part of Modus from the part which handle things like invoking Docker or file IO, since the latter will not compile when targeting WASM. \Cref{sec:playground-impl} discusses how this is done, and the finished playground can be accessed at \url{https://play.modus-continens.com/}.
