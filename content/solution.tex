\chapter{Solution \& Implementation}
\label{chap:solution-and-impl}

\hyperref[chap:problems]{The last chapter} outlined several problems that need to be addressed in extending Modus. This chapter revisits each of them and discusses our solution and some implementation detail.

This chapter contain references to Modus code, which can be accessed at the following URL:

{%
  \color{link}\ttfamily\noindent\centerline{%
  \href{https://github.com/modus-continens/modus/tree/9c06c5bc3b35c92c03ab89a129cf6eddcab92a88}{https://github.com/modus-continens/modus/tree/9c06c5b}}%
}

This link points to the point-in-time snapshot of the code at the end of this project. If the above link is no longer accessible, two of the files---\lstinline{modus-lib/src/imagegen.rs} and \lstinline{modus/src/buildkit_frontend.rs}---has been \hyperref[sec:modus-source-listing]{partially included as appendixes} in this report.

\section{Supporting Builtin Predicates}
\label{sec:impl-supporting-builtin-predicates}

To recap, builtin predicates are predicates that represent basic container building functions or common logical statements. They are part of Modus and can be directly used by any Modusfile.  Since builtin predicates can not be expressed in the Modus language itself, we implement them by creating an interface that can be used by the proof solver to resolve them.

\subsection{A Common Interface for Builtin Predicates}

In \Cref{sec:datalog} we discussed the working of the proof resolution in Datalog, which applies to Modus as well. Most importantly, a literal like \lstinline[mathescape]{p($t_1$, $\ldots$, $t_n$)}, where each $t_i$ is either a constant or a variable can be proven by finding a rule of \lstinline{p} and a substitution $\theta$ such that, after applying the substitution to both the rule and $t_1, \ldots, t_n$, the head of the rule equals the to-be-proven literal, and the body of the rule can again be proven. We denote the process of finding such a substitution $\theta$ as \emph{unification}, and we say that a predicate can be \emph{unified} with a rule if $\theta$ exist.

This unification process allows a predicate to place equality constraints on a variable anytime it is used. For example, the rule \lstinline[mathescape]{p("a", "b") :- $\ldots$} can be unified with \lstinline{p("a", x)} by using the substitution $\{\texttt{x} \to \texttt{"b"}\}$, whereas \lstinline{p("b", x)} can not be unified with the rule, because there is no way to make $(\texttt{"b"}, x)$ equal to $(\texttt{"a"}, \texttt{"b"})$.

Since builtin predicates are not implemented in the Modus language itself, they do not have such inference rules to be unified against. However, we can still leverage unification to encode constraints on arguments of a builtin predicate. We will now use an example to motivate this approach.

{%
\def\tA{\texttt{A}\xspace}%
\def\tB{\texttt{B}\xspace}%
\def\tC{\texttt{C}\xspace}%
%
Consider the predicate \lstinline{string_concat(A, B, C)}, true iff $\tA \concat \tB = \tC$ \ (``$\concat$'' denotes the string concatenation operation). When the proof resolution process sees a use of this predicate, it needs to decide whether the predicate is true, and what additional constraints this predicate imposes upon \tA, \tB and \tC. There are, in general, 3 possible ways this can manifest:

\begin{enumerate}
  \item All three arguments are grounded---they are constants, or are variables that have already been substituted into a constant.
  \item Two of the arguments are grounded, and the remaining one is an ungrounded variable.
  \item Only zero or one of the arguments are grounded.
\end{enumerate}

In the first case, it is possible to immediately tell if the constraints are satisfied. If they are, the resolution process can simply continue on, as if a substituted, fully grounded literal of the \lstinline{string_concat} predicate is an unconditionally true fact. Otherwise, it must consider this literal invalid, as if there is no facts or rules that can be unified with this literal, and backtrack.

In the second case, it is not possible to immediately conclude whether the constraints are satisfied or not. However, since two of the three arguments are available, there is always enough information to reduce this literal into an equality constraint on the remaining argument, or to conclude that the literal is false. This can further manifest in two ways:

If the ungrounded argument is \tC, this literal essentially forces the variable in \tC to be substituted to $\tA \concat \tB$, which is just another known constant at this point. The result is as if there is a ground fact \lstinline[mathescape]{string_concat(A, B, $s$)} where $s = \tA \concat \tB$ in the program. The proof process can then unify the to-be-proven literal with this hypothetical ground-fact and continue as usual.

If the ungrounded argument is either \tA or \tB, the unknown argument can be computed by either removing a suffix \tB from \tC, or removing a prefix \tA from \tC. If the prefix or suffix specified is not in \tC, the literal is false. Otherwise, a substitution from the unknown variable to the computed string can be enforced in the same manner as before.

In the third case, where only zero or one of the arguments are constant or ground variables, the set of ground facts that this literal is equivalent to is either potentially large (in the case where only \tC is grounded) or infinite (in any other case). Instead of trying to unify with a large or infinite set of facts, we want to defer the evaluation of this literal until at least two of the arguments have been resolved to a constant. This does not mean that the literal is immediately false: the proof resolution process must take the possibility of this into account and evaluate the literal again when the groundness of the arguments matches either of the two prior cases.

}

From this example, we can see that a possible interface for builtin predicates is to create constraints in the form of phony ground facts (unification candidates). This is most similar to how user-defined predicates are resolved, and is therefore likely to require minimal modification to the proof solver. However, the case where not enough information is present to generate a ground fact needs to be checked and handled.

In \Cref{sec:using-datalog-to-docker} we mentioned that Modus imposes the restriction that a query can not generate an infinite number of images. This is enforced in the existing Modus prototype by limiting how variables interact with predicates when they are used in arguments. All user-defined predicates are attached with a \emph{groundness signature}, which determines which of its arguments can receive an ungrounded variable when a literal of the predicate is unified. When this is not the case, the literal is kept as-is, and other literals in the current rule is considered first. This effectively defers the predicate evaluation until the required variables in its argument have a known value, and if these literals are the only ones left, the rule can not be applied, which may eventually cause the resolution to fail with a groundness error, if no other alternative rules can be used. For example, the rule \lstinline{p(x, y) :- q(x).} can not be used for the query \lstinline{p(x, y)} because the groundness check requires that the argument \lstinline{y} is grounded.

This can be generalized and used as an effective way to implement the deferring of builtin predicates mentioned before. Every builtin predicates will have its own groundness signature, which prevents it from being used until enough information is present. This can also be used to enforce that arguments to container build instructions such as \lstinline{run} must be grounded, since the full details of the build steps must be determined eventually in order to execute them.

While not all builtin predicates require complex logic like \lstinline{string_concat}, the outlined interface above is flexible enough to implement all proposed builtin predicates (and operators), and is likely to generalize well to other predicates in the future. Groundness signatures also partially addresses the decidability problem with builtin predicates. For example, in our implementation, \lstinline{number_lt(a, b)} requires both \lstinline{a} and \lstinline{b} to be grounded, as otherwise there is an infinite set of possible facts. This means that, while \lstinline{number_lt} itself represents an infinite set of ground facts, any Modus program can only observe a finite subset of them, as long as no other builtin predicate allows generating an unbounded set of new constants (which is true for \lstinline{number_lt} with this groundness requirement).

\subsection{Implementation}

All builtin-related code is in \lstinline{modus-lib/src/builtin.rs}. The interface proposed above is implemented as a trait \lstinline{BuiltinPredicate}. It contains the following two core functions:

\begin{itemize}
  \item \mintinline{rust}{fn arg_groundness(&self) -> &'static [bool]}

  This returns the groundness signature. Each element of the array denotes whether the argument is allowed to be ungrounded.

  \item \mintinline{rust}{fn apply(&self, lit: &Literal) -> Option<Literal>}

  This returns the grounded fact generated by a particular invocation, or \None to force the predicate to be false (\eg \lstinline{string_concat("a", "", "")}).
\end{itemize}

A list of such predicates is declared in this file and used in \lstinline{sld.rs}, which contains the proof solver. The solver calls a function that determines if a to-be-proven literal is a builtin predicate, and returns the generated ground fact if this is the case. The ground constraint is then treated as a fact and the to-be-proven literal is unified against it.

The \lstinline{string_concat} predicate is implemented as 3 separate predicates with the same name, but different groundness signatures. Each of these predicate handles the case where one particular argument is ungrounded. The proof solver will select the first predicate with compatible groundness. The \lstinline{apply} function computes what the missing argument should be, and simply return the input literal with that argument replaced with the computed value, or \None in the case where the specified prefix or suffix is inconsistent with the third argument.

Predicates that represent build instructions and otherwise entails no logical constraints on their own, such as \lstinline{run}, are implemented with a macro---\lstinline{intrinsic_predicate!}---that generates trivial implementation based on a given groundness signature. In these cases, \lstinline{apply} is implemented simply as an identity function. Most logic predicates like number or SemVer comparisons are implemented also as predicates that require all arguments to be grounded. The logical condition is checked in \lstinline{apply}, and the input literal is also returned as-is if the condition is true, or \None if otherwise.

\section{Embedding Operators into the Build Tree}

Recall that operators are predicates that can be applied to a sub-expression, in the form of \lstinline[mathescape]{expr::operator(v$_1$, $\ldots$, v$_n$)}. Aside from this, they should behave like any other predicates in the proof solving process, handling variables and potentially ungrounded arguments. This can be implemented by mapping operators to existing Datalog primitives.

A straightforward mapping would be to translate operators into begin/end predicates that can be paired up uniquely. Uniqueness can be ensured by adding an identifier argument. For example, the expression

\begin{lstlisting}[mathescape]
  (p$_1$(u$_1$, u$_2$), p$_2$(w$_1$, w$_2$))::operator$_1$(v$_1$, v$_2$)::operator$_2$(v$_3$, v$_4$)
\end{lstlisting}

can be translated into

\begin{lstlisting}[mathescape]
  _operator$_2$_begin("1", v$_3$, v$_4$),
  _operator$_1$_begin("2", v$_1$, v$_2$),
  p$_1$(u$_1$, u$_2$),
  p$_2$(w$_1$, w$_2$),
  _operator$_1$_end("2", v$_1$, v$_2$),
  _operator$_2$_end("1", v$_3$, v$_4$)
\end{lstlisting}

This representation is both easy to work with and flexible enough, and it means that any proof-resolution code does not have to be aware of operators. It allows any variables in $\texttt{v}_i$ to participate in the proof resolution, just like any other predicates, and the identifier ensures that it is easy to find the matching predicates even when the same operator is used two times.

This translation is implemented in \lstinline{modus-lib/src/translate.rs}, which is called after parsing a Modusfile to generate an \emph{intermediate representation} (IR) to be consumed by the proof solver. This IR translation includes, among other logic implemented by \redacted{Chris}, translating operator applications into the representation described above.

\section{A BuildKit-based Backend for Modus}
\label{sec:impl-buildkit-backend}

In \Cref{sec:problem-buildkit-backend} we proposed the creation of a new backend for Modus based on BuildKit. This section will address the implementation for this new backend, and any challenges involved in it.

The BuildKit framework exists both as a standalone daemon separate from Docker, known as \texttt{buildctl}, and also as an embedded part of Docker, supported experimentally by \texttt{docker build}. Integrating with the daemon version of BuildKit would require users to install BuildKit separately as it is not distributed with Docker. Alternatively, Modus can leverage the BuildKit support in \texttt{docker build}. This is made possible by a Docker feature known as \emph{custom Dockerfile implementations}. Quoting from official documentation:

\begin{displayquote}[\cite{dockerfile-reference}]
  ``The syntax directive defines the location of the Dockerfile syntax that is used to build the Dockerfile. The BuildKit backend allows to seamlessly use external implementations that are distributed as Docker images and execute inside a container sandbox environment.''
\end{displayquote}

Ideally, this would mean that all the user has to do to use Modus is just to create a Modusfile with the first line being \lstinline[mathescape]{# syntax=}\textit{modus image}, then add Modus definitions to the file and simply run \lstinline{docker build . -f Modusfile}. While this may work for a limited set of cases, if there are errors in the user's Modusfile, \texttt{docker build} will not forward any printed error messages from Modus, and will instead show a one-line generic ``failed to solve with frontend \ldots'' message. Given that Modus is a proof-based system and non-obvious mistakes can easily occur, this is not likely to be a good user experience.

More importantly, as mentioned in \Cref{sec:problem-buildkit-backend}, the BuildKit API currently only supports specifying one output image. With Modus running inside Docker this way, there is very little flexibility for a workaround of this issue, as Docker will only output and tag one final image. This means that it is not possible to build queries that generates multiple output images this way. While this problem still needs to be solved separately even having Modus as a standalone binary invoking Docker, this already rules out the possibility of using Modus solely via this Docker feature.

However, considering the additional friction of having to install \texttt{buildctl}, and that it does not provide much additional benefit over Modus invoking \texttt{docker build}, we decided to still make use of this custom Dockerfile implementations feature. We will create a BuildKit frontend for Modus in this way, but it will only handle actual container building tasks, and will take a JSON build graph as input, passed in by the Modus executable running outside of Docker. The outside Modus executable will be responsible for printing any error if they exist, and, as we will see later, handles the building of multiple images.

\subsection{Generating Build Graph}
\label{sec:generating-build-graph}

In order for Modus to actually carry out a container build, whether via BuildKit or Dockerfiles, it needs to generate a build graph from the proof tree. Since our backend will be based on BuildKit, we will use an internal representation (\ie the JSON input to our BuildKit frontend mentioned previously) that can be mapped to BuildKit graph in a trivial manner.

\begin{figure}[t]
  \centering\hspace*{-20pt}%
  \begin{subfigure}{\dimexpr 0.5\textwidth + 18pt\relax}
    \begin{minted}{prolog}
      app(base) :-
        from(base),
        run("true"),
        print("hello"),
        (
          from("alpine"),
          run("echo inline sub-image > /tmp/txt")
        )::copy("/tmp/txt", "/").

      print(x) :- run(f"echo ${x}").
    \end{minted}

  \end{subfigure}\kern-7pt%
  \begin{subfigure}{\dimexpr 0.5\textwidth + 28pt\relax}
{%
\footnotesize%
\dirtree{%
.1 app(``alpine'').
.2 from(``alpine'').
.2 run(``true'').
.2 print(``hello'').
.3 run(``echo hello'').
.2 \_operator\_copy\_begin(``1'', ``/tmp/txt'', ``/'').
.2 from(``alpine'').
.2 run(``echo inline sub-image > /tmp/txt'').
.2 \_operator\_copy\_end(``1'', ``/tmp/txt'', ``/'').
}
}
  \end{subfigure}

  \caption{Example Modusfile and its proof tree, with raw operator predicates.}
  \label{fig:example-raw-proof-tree}
\end{figure}

\Cref{fig:example-raw-proof-tree} shows an example Modusfile and its resolved proof tree, with the operator translation applied. Builtin predicates like \lstinline{from} and \lstinline{run} can be mapped directly into build nodes, and a recursive algorithm can be used to handle potentially nested operators. Since operators can be used to surround arbitrary expressions, which can resolve into multiple siblings in a proof tree, the input to this algorithm needs to be an array of tree nodes. The algorithm would traverse this tree in a depth-first manner, flatten all build instructions that it encountered and translate them to some representation of BuildKit instructions, and when it finds an operator predicate, run itself on the nodes surrounded by the operator begin and end predicates. A brief pseudo-code for this algorithm is shown in \Cref{alg:generating-build-graph} (page~\pageref{alg:generating-build-graph}).

\begin{algorithm}[ht]
  \begin{algorithmic}[1]
    \Function{process\_image}{\texttt{slice}: array of proof tree nodes}
      \LComment{Takes in an array of siblings in a proof tree and processes it, treating everything as part of an image predicate.}
      \LComment{We initialize a new state for the current image:}
      \State $N \leftarrow \texttt{None}$
      \Comment{$N$ stores the last build node generated.}
      \State $S \leftarrow \text{default state}$
      \Comment{$S$ tracks other states like working directory.}

      \State \Call{process\_children}{\texttt{slice}}

      \Return $N$

      \Function{process\_children}{\texttt{slice}: array of proof tree nodes}
        \LComment{Processes the array \texttt{slice}, possibly changing the outer $N$ and $S$ variables.}
        \State $i \leftarrow 0$
        \Comment{Index into the \texttt{slice} array}

        \While{$i < \text{length of }\texttt{slice}$}
          \State $\texttt{node} = \texttt{slice}[i]$
          \If{\texttt{node} is an operator begin predicate}
            \State $j \leftarrow i + 1$
            \While{$\texttt{slice}[j]$ is not the corresponding operator end}
              \State Increment $j$
            \EndWhile

            \State Process this operator, possibly updating $S$
            \State For image-taking operators, call \Call{process\_image}{\texttt{slice}$[i+1:j]$}
            \State For layer-taking operators, call \Call{process\_children}{\texttt{slice}$[i+1:j]$}
            \State Restore any changes to $S$
            \State $i \leftarrow j$
          \ElsIf{\texttt{node} is a builtin}
            \State Process \texttt{node}, possibly updating $N$
          \Else
            \State \Call{process\_tree}{\texttt{node}}
          \EndIf
          \State Increment $i$
        \EndWhile
      \EndFunction

      \Function{process\_tree}{\texttt{node}}
        \If{\texttt{node} is a cached image predicate}
          \State \textbf{assert:} $N$ is \texttt{None}
          \Comment{Multiple image predicates are not allowed}
          \State $N \leftarrow$ cache$[\texttt{node}]$
        \Else
          \State $\texttt{slice} \leftarrow \texttt{node.children}$
          \If{\texttt{node} is an image predicate}
            \State \textbf{assert}: $N$ is \texttt{None}
            \State $N \leftarrow$ \Call{process\_image}{\texttt{slice}}
            \State cache$[\texttt{node}] \leftarrow N$
          \Else
            \State \Call{process\_children}{\texttt{slice}}
          \EndIf
        \EndIf
      \EndFunction
    \EndFunction

  \end{algorithmic}
  \caption{Algorithm which turns a proof tree into a build graph.}\label{alg:generating-build-graph}
\end{algorithm}

When multiple images are induced by a query, proof resolution in Modus will generate a proof tree for each image separately. Each proof tree is a self-contained tree of build instructions and operators, and in principle can be processed separately. However, special caching of repeated literals should still be implemented, so that image predicates that are used multiple times across the different outputs are guaranteed to be built once, regardless of what BuildKit ends up deciding to cache. This is done by storing literals with the final node of its image in a hashmap, and checking this map each time a literal is encountered.

This algorithm is implemented in \lstinline{modus-lib/src/imagegen.rs}. The main function is \lstinline{build_dag_from_proofs}, which takes in a set of solved proof trees, one for each output image, and output a \lstinline{BuildPlan} which builds all the images. It calls \lstinline{process_image} on each proof tree to generate the image node (or use the cached node in the hashmap if it has been generated before, possibly because it is a dependency of another output).

To handle operators like \lstinline{::in_workdir} or \lstinline{::in_env}, which applies to a set of layers within an outside image, actual processing happens in a separate function \lstinline{process_children}, which can be called recursively on tree arrays which do not represent a new image. A context structure initialized in the beginning of \lstinline{process_image} is used to store the state of the current image, such as the last node generated, or the current working directory, and is modified by \lstinline{process_children} and other functions which it invokes. When an operator like \lstinline{::in_workdir} or \lstinline{::in_env} is encountered, the context is changed accordingly, and restored back when the recursion for this operator exits.

\afterpage{\FloatBarrier} % otherwise the algorithm is placed too far away.

\subsection{Invoking BuildKit}
\label{sec:invoking-buildkit}

In order to leverage the BuildKit support embedded within Docker, we will use the ``custom Dockerfile implementations'' Docker feature mentioned in the beginning of this section~(\ref{sec:impl-buildkit-backend}).

Specifically, we will create a BuildKit frontend for Modus. The frontend will not be user-facing, but will expect as input the build graph generated earlier in \Cref{sec:generating-build-graph}. Once Modus has generated a build graph from a given Modusfile, it will invoke \lstinline{docker build} in BuildKit mode by setting the environment variable \lstinline{DOCKER_BUILDKIT=1}, using a generated, temporary Dockerfile that will cause \lstinline{docker build} to invoke our BuildKit frontend with the input we provide. This is done by using the ``\lstinline{# syntax=}'' directive with a custom Modus image, then writing the build graph as a JSON following this line. The BuildKit frontend will be able to read this Dockerfile as input, and therefore get back the build graph by parsing the JSON.

The code for this frontend is contained in \lstinline{modus/src/buildkit_frontend.rs}. It implements an almost-direct translation from our internal build graph to BuildKit's format. Interactions with BuildKit is done via an open-source Rust library rust-buildkit~\cite{rust-buildkit}.

A slight difference between our internal build graph and BuildKit's input is that image properties like working directory, entrypoint and environment variables are not part of BuildKit's build graph. These properties can only be set for the final image by returning a single struct containing all the desired properties, and there is no inheritance of properties. To simplify our design, we decided to include property-setting nodes in our internal build graph. This means that inheritance is handled naturally, and will also come handy if we wish to transpile the internal graph into pure Dockerfile. In our BuildKit frontend, each internal node is mapped to a BuildKit node together with the combined set of image properties. For an internal node like \lstinline{SetEnv}, the BuildKit node is passed-on as-is, but the image property set is updated. When the \lstinline{Run} node is encountered, our frontend ensures correct behaviour regarding existing image properties by \eg setting the environment variables in the corresponding BuildKit node according to the current image property set. To preserve Dockerfile's inheritance semantic, we also fetch the properties of all base images (\ie those introduced by \lstinline{from}) via a BuildKit API, and use them to initialize our property set.

Since we are able to fetch the properties of an existing image, including its working directory, we can enhance the semantic of \lstinline{::copy}. In Dockerfiles, relative paths do not work when copying from another intermediate image---they are always interpreted as relative to \lstinline{/}. In our implementation of \lstinline{::copy}, when a relative path is used as the source path, we will resolve them relative to the working directory property of the source image.

Since the BuildKit frontend need to be distributed as a Docker image, a CI workflow has been created on GitHub to build and publish this image each time the code changes. \lstinline{.github/workflows/rust.yml} contains the code for this workflow. Depending on an external image also means that there can be problems when Modus is updated in a way that breaks compatibility between different versions of the frontend image. To ensure that Modus always uses the image matching its version, the CI workflow will tag the image with the current commit hash. Modus will embed its own commit hash at build time through the use of a Rust build script at \lstinline{modus/build.rs}, and thus it will always be able to use the image matching its version. These images are stored on GitHub container registry under \href{https://github.com/modus-continens/modus/pkgs/container/modus-buildkit-frontend}{\color{link}\lstinline{ghcr.io/modus-continens/modus-buildkit-frontend}}.

\subsection{Handling Multiple Output Images}
\label{sec:handling-multiple-output-images}

Even though BuildKit takes a graph as input and automatically parallelizes when possible, only one node can be marked as the final output image. This means that queries that generates multiple images would not work without additional workaround.

The simplest solution would be to build each output images individually. If they are built one after another, shared layers would not be duplicated because they will be cached by BuildKit. However, this means that any independent steps in the different outputs will only execute sequentially, which is not maximally efficient.

Ideally, this problem would be addressed upstream, with BuildKit adding a feature to support multiple outputs. However, for the purpose of this project, we decided to adopt a workaround: when a query generates multiple images, we will include all of them in the build graph, and create an extra, placeholder image as the output. This placeholder image does not serve any purpose on its own, and can simply be an empty image. In order to force BuildKit to build all of our desired images in parallel, we will make this placeholder image depend on all the original output images. This is done via the ``mount'' functionality: we create a run layer that does nothing, but mounting the original output images in subdirectories like \lstinline{/_0}, \lstinline{/_1}, and so on.

Once this build is done, Modus will invoke \lstinline{docker build} again for each of the intended output images. This time, instead of building the placeholder image, the node for the intended output images will be marked as the output in each invocation. This is not expected to cause any unnecessary rebuilds, as all the layers should already exist in the build cache following the first build.

This solution is not ideal: due to quirks in the BuildKit-Docker integration, for each output image, a checksum has to be re-computed in order for BuildKit to ``export'' the image to Docker. In practice, this may cause a slight performance penalty of 0.5 to \SI{1}{s} per output image. However, this overhead may be avoided if BuildKit exposes an appropriate API in the future.

% \todo{resolving stage (should I include it? I feel like it's too much implementation-detail and boring, and also the report is quite long already)}

\subsection{Implementing \texttt{::merge}}

Recall that the \lstinline{merge} operator squashes a set of \lstinline{run}, \lstinline{copy} and \lstinline{::copy} into one image layer. There is no equivalent mechanism in BuildKit that allows executing several run and copy operations in one node, and therefore this operator will be implemented by combining all operations scoped within the operator into a single shell script, which will be executed as a single run layer. For copy operations, the relevant source (which can be an image or the local build context) is mounted to a temporary directory during the running of the script, and the \lstinline{cp} shell command is used in the script to execute the actual file copying.

When strings from Modusfile are embedded in this script (for example, the source and destination of \lstinline{copy}), they are escaped following the syntax for \lstinline{sh}: the character \lstinline{'} is escaped as \lstinline{\'}, and everything else is surrounded inside a pair of \lstinline{'}. We used a third-party library shell-escape~\cite{shell-escape} for the implementation of this.

In Dockerfile, when the source of a \lstinline{COPY} instruction is a directory, Docker will copy the content of the directory, not the directory itself, into the destination. This behaviour is also implemented in BuildKit, and therefore also Modus. In other words, for the following Modusfile, the image \lstinline{out} will actually contain \lstinline{/tmp/file}, rather than \lstinline{/tmp/dir/file}.

\begin{minted}{prolog}
  s :-
    from("alpine"),
    run("mkdir /tmp/dir && echo 'hello' > /tmp/dir/file").
  out :-
    from("alpine"),
    s::copy("/tmp/dir", "/tmp").
\end{minted}

To ensure that this behaviour doesn't change under \lstinline{merge}, we use a special syntax of \lstinline{cp} to copy the content when the source path specified is a directory. When the last component of the source path is \lstinline{.}, \lstinline{cp} will copy the content of the source directory directly into the destination, including all hidden files (unlike using shell expansion like \lstinline{dir/*})~\cite{cpdot}.

\section{Docker Integration}

To improve the ease-of-use of Modus images, we proposed that Modus should implement a subset of Docker commands such as \lstinline{run} and \lstinline{ls} in \Cref{sec:problem-docker-integration}, and accept Modus literals in place of image arguments. To make this possible, Modus has to be able to associate individual Docker images with the Modus literal they are built with.

One way this can be done is by using image labels, which are arbitrarily key-value pairs that can be stored in an image's metadata. We decided to use the label key \lstinline{com.modus-continens.literal}, and store the Modus literal in string representation as the value. Docker supports finding an image by label both in its command line interface and its REST API.

However, while a solution has been proposed, the remaining part of this implementation has not been completed at the time of writing. For the purpose of this project, we implemented a JSON output option that allows an external script to find out the image hash corresponding to newly built predicates after a \lstinline{modus build} invocation.

\section{Improving Installation via Binary Distribution}
\label{sec:impl-binary-dist}

In \Cref{sec:invoking-buildkit}, we discussed how a CI workflow on GitHub is used to build the image for the BuildKit frontend. The same workflow can be extended to publish the built binary artifacts. To ensure that our binary is maximally compatible, we built Modus under the \lstinline{musl-cross}~\cite{musl-cross} toolchain. This toolchain also allowed us to cross-compile Modus to support ARM-based Linux platforms in the same workflow.

At the end of the workflow, a special GitHub Action is used to publish a draft release on GitHub with the binaries attached. This release can be easily turned into an official release in one click---all building tasks and tests are automated as much as possible.

\section{Building the Modus Playground}
\label{sec:playground-impl}

As mentioned in \Cref{sec:more-doc-and-playground}, to better demonstrate the working of the Modus language, an online playground will be created which will show the proof tree of a given Modusfile and query. In order to build Modus for the WASM target, code that is specific to system operations such as invoking Docker or reading files must be separated from code that \eg parses a Modusfile and does proof resolution. This is done by moving all parsing and logic code into a separate crate \lstinline{modus-lib}. This crate will be stored alongside the main \lstinline{modus} crate in the same repository, and the two crates will always have matching versions to simplify maintenance.

To create our WASM app, we followed the approach outlined in the MDN WebAssembly tutorial~\cite{mdn-wasm-rust}. A simple HTML page was created with the ability to input Modus code and query, and the proof tree is printed on the page as the Modusfile is updated.
% The ansicolor~\cite{ansicolor} library is used to colorize error messages in the same way as they are shown in the terminal.
The final website can be accessed at \url{https://play.modus-continens.com/}.

\phantomsection%
\section*{Summary}%
\addcontentsline{toc}{section}{Summary}

In the previous chapter, we identified various problems and improvements regarding Modus, and we discussed how they are addressed and implemented in this chapter. The majority part of this project has been devoted to these two areas, and we have been able to turn Modus into a tool ready for realistic use-cases. Following these developments, Modus has been released to the public, as mentioned earlier in the \hyperref[chap:introduction]{Introduction}. In addition to all the aforementioned areas, this project also contributed in various smaller ways to improve various aspect of Modus. These contributions include edits to the official Modus website, the creation of a syntax highlighting grammar for Modusfile used on the website and inside documentation, \etcetra. Throughout the development process, various tests are also written to ensure correctness, which we will explore in \hyperref[chap:testing]{the next chapter}. Aside from development, this project also evaluated Modus on existing projects, which will be discussed in \Cref{chap:evaluation}.
