\chapter{Evaluations}
\label{chap:evaluation}

Aside from improving Modus itself, core goals of this project also include evaluating Modus on various other real-world applications. For this project, a corpus made of six popular images on Docker Hub was selected. Their build systems (\ie Dockerfiles along with other scripts) was rewritten to use Modus, and results in terms of code conciseness and build performance was compared with the original.
%<docker-hub-evaluation.tex>
We conclude that Modus reduced the code size of container image build systems by \dockerHubCodeReductionLines while introducing only a negligible performance overhead, and preserving the original image size and space efficiency, as measured by \texttt{dive}~\cite{diveURL}.

This chapter will start by discussing how the corpus was selected, then moving on to the challenges associated with creating the Modus equivalent of these projects. We will then present our code reduction and performance metrics, along with the exact setup used to generate these metrics, and finish by describing the approach to validating that the image produced matches that of the original.

\section{Corpus Selection}

To build our corpus, this project considered top images on Docker Hub~\cite{docker-hub}, as ranked by its ``Suggested'' filter, which closely follows downloads, and selected the first six which used different sequences of commands for templating. Under this rule, selecting MySQL filtered out Python and PostgreSQL. It was also intended that projects that either do not use parameters in their tags or build only one container image are to be ruled out, as the focus of Modus is on parameterised builds, but this filter was not applicable to any of the top projects.

We have no reason to believe that this selection process introduces bias with respect to the object of this study---namely, builds conditioned on parameters.  Using popular examples is common practice in empirical work. Any project considering Modus would do so to adopt Modus' feature set precisely because Modus promises to help them speed up or ease the maintenance of their build.

{%
  \newcommand\proj[2]{\href{#1}{{\color{link}\textbf{#2}}}}%
  \begin{table}[t]
    \centering\begin{tabular}{lcr}
      \toprule
      \multicolumn{1}{c}{\textbf{Project}} & \multicolumn{1}{c}{\textbf{Templating Method}} & \multicolumn{1}{c}{\textbf{Outputs}} \\
      \midrule
      \proj{https://github.com/tianon/docker-brew-ubuntu-core}{Ubuntu}     & bash        &  6 \\
      \proj{https://github.com/docker-library/redis}{Redis}                & sed + awk   &  9 \\
      \proj{https://github.com/nginxinc/docker-nginx}{Nginx}               & sed         &  8 \\
      \proj{https://github.com/nodejs/docker-node}{NodeJS}                 & sed + awk   & 32 \\
      \proj{https://github.com/docker-library/mysql}{MySQL}                & awk + jq    &  4 \\
      \proj{https://github.com/traefik/traefik-library-image.git}{Traefik} & envsubst    &  4 \\
      \bottomrule
    \end{tabular}
    \caption{Corpus Descriptive Statistics.}
    \label{table:corpus}
  \end{table}
}


Porting each project to Modus requires understanding the existing build and, in turn, substantial manual effort. The six projects we selected collectively cover an interesting subdomain of the container build system space. \Cref{table:corpus} summarises our corpus.

\section{Porting to Modus}
\label{sec:porting-to-modus}

Project maintained by Docker Hub usually follows a common approach---these projects generate a list of possible configurations, either hardcoded into the build source, inferred from directory names in the file system, or downloaded from project's official website, then generate Dockerfiles for each configuration via a template. Porting them to Modus involves translating the Dockerfile templates into a single Modusfile that takes the same build steps as those of the templated Dockerfiles for a given set of parameters.

\begin{listing}[t]
  \begin{minted}{bash}
      if [ "$variant" = '32bit' ]; then
        sed -ri \
          -e 's/(make.*) all;/\1 32bit;/' \
          -e 's/libc6-dev/libc6-dev-i386 gcc-multilib/' \
          "$dir/Dockerfile"
      fi

      case "$version" in
        5)
          gawk -i inplace '
            $1 == "##</protected-mode-sed>##" { ia = 0 }
            !ia { print }
            $1 == "##<protected-mode-sed>##" { ia = 1; ac = 0 }
            ia { ac++ }
            ia && ac == 1 { system("grep -vE \"^#\" old-protected-mode-sed.template") }
          ' "$dir/Dockerfile"
          ;;
      esac
      sed -ri -e '/protected-mode-sed/d' "$dir/Dockerfile"

      # TLS support was added in 6.0, and we can't link 32bit Redis against 64bit OpenSSL (and it isn't worth going to a full foreign architecture -- just use i386/redis instead)
      if [ "$version" = '4.0' ] || [ "$version" = '5' ] || [ "$variant" = '32bit' ]; then
        sed -ri \
          -e '/libssl/d' \
          -e '/BUILD_TLS/d' \
          "$dir/Dockerfile"
      fi
    \end{minted}
    \caption{An extract of Redis' templating script.}
    \label{lst:redis-templating-script}
\end{listing}

Understanding the behaviour of the templates in our corpus was not always straightforward. In the case of Redis, their templating script, as partially shown in \Cref{lst:redis-templating-script}, contained commands that replaces certain strings anywhere in the template. These replacements have two purposes: (1) to change some of the apt install packages to 32bit when required and (2) to change existing instructions in the Dockerfiles which themselves do source file patching. The patching instructions disables some Redis functionality irrelevant under Docker, and the templating script changes these instructions to make them work for different versions. Specifically, the template contains valid instructions for newer versions of Redis, surrounded by the placeholder \lstinline{##<protected-mode-sed>}. The templating script uses a sed script to replace these placeholders and anything in between with a string from a separate file when the version is 5.

The Nginx project uses two template Dockerfiles, for Debian and Alpine distribution respectively. Their templating script contains most of the logic, written in bash, that sets the value of the substitutions in their Dockerfiles. The script manipulates strings and constructs sets of packages to install, inserted into the Dockerfile in the form of a space-separated string that gets passed to \texttt{apt}. Porting this logic into a Modusfile significantly reduces the build code needed from 169 lines of bash script to 58 lines of Modus logic (ignoring container build instructions, which are the same in both their script and the Modusfile created by this project).

The Traefik project contains Dockerfiles that build scratch images (images without a distribution) by copying binaries from the Alpine images built by other Dockerfiles in the same project. This means that these Dockerfiles must be built in a particular order, and therefore a custom script was created to build the images in order, rather than using a single GNU Parallel invocation, as we did for the other benchmarks (more in \Cref{sec:experiment-setup}). Our Modusfile did not have to explicitly consider such constraints, as Modus, out of the box, ensures that all dependencies are built in the correct order.

Ubuntu Dockerfile does not download and extract tarballs of the rootfs (the root filesystem of the distribution). Instead, its update script performs this task outside of Docker. To better utilize Modus' expressivity and parallelism, we include the download and extraction commands for all the tarballs in our Modusfile as intermediate build stages. This removes the need for an external script for parts of the build process, but does means that our build time would be higher because of this. The average time taken by Ubuntu's shell script to download all tarballs is \SI{3.34}{s} (3.28--3.41), and therefore we expect our build time to be reduced by this amount if we had downloaded the tarballs before running Modus.

\section{Modus' Code Reduction}

We now quantify and compare the size of the build systems of our corpus to their Modus ports. The key finding here is that Modus reduces build code size by \dockerHubCodeReductionLines, on average, over our corpus in lines of code, by reducing repetition and avoiding scripting.

Modusfile and Dockerfile have different grammar, indenting and line-wrapping conventions. To make the comparison more meaningfully represent actual semantic content of the code, a number of filters were applied to both Docker Hub's source code and our Modusfile, before code size metrics are counted. Specifically, comments and empty lines are removed, consecutive whitespaces are replaced with one single character, and templating delimiters like \lstinline|{{| are also removed. The script \lstinline{fair-codesize.sh} (\hyperref[appendix:fair-codesize]{reproduced in the appendix}) implements this filtering.

\Cref{tab:docker-hub-eval-code-size} shows our code reduction results,
calculated in two ways.  \Cref{tab:docker-hub-eval-code-size-lb} only counts the
Dockerfile templates, not scripts that use them to generate Dockerfiles, in the Templating columns
and only Modusfiles in the Modus columns.  To the counts in
\Cref{tab:docker-hub-eval-code-size-lb}, \Cref{tab:docker-hub-eval-code-size-ub}
adds the sizes of all templates and scripts used to build the images in the
Templating columns, and any scripts needed to generate version lists (but not
templating) in the Modus columns.  We present both tables because the
definition of build code is not well established.  One view is that a build
system consists only of the Dockerfile and Modusfiles.  Another view is that a
build system includes all the version fetching and templating scripts.  Further
complicating matters is the fact that some of these scripts may additionally
perform tasks unrelated to building their project.  Thus, we present these two
tables to establish lower and upper bounds on the true size of each project's
build system.

In \Cref{tab:docker-hub-eval-code-size-lb}, Ubuntu's corpus does not use
templates, but a bash script that prints its Dockerfiles, so we did not report
its template sizes. Nginx's templating script contains most of its
build logic, written in bash, and it simply writes results into its templates.
The script manipulates strings and constructs sets of packages to install,
inserted into the Dockerfile in the form of a space-separated string that get
passed to \texttt{apt}. Porting this logic into a Modusfile substantially
reduces the build code needed from 169 lines of bash script to 58 lines of
Modusfile (ignoring docker build instructions in either case). This accounts for
the swing in the results for Nginx across \Cref{tab:docker-hub-eval-code-size-lb} and
\Cref{tab:docker-hub-eval-code-size-ub}.

\begin{framed}
	\noindent Modus reduces build system code size by \dockerHubCodeReductionLines on average in lines and \dockerHubCodeReductionWords
	reduction in words.
\end{framed}

\begin{table*}[t]
  \begin{subtable}{\columnwidth}
    \centering%
    \begin{tabular}{lrrrrrrrrr}
      \toprule
      {} & \multicolumn{3}{c}{Templating} & \multicolumn{3}{c}{Modus} \\
      \cmidrule(lr){2-4} \cmidrule(lr){5-7}
      \textbf{Project} & \textbf{CR} & \textbf{Words} & \textbf{Bytes} & \textbf{CR} & \textbf{Words} & \textbf{Bytes} \\
      \midrule
      \textbf{mysql} & 162 & 600 & 6398 & 176 (+08.6\%) & 586 (\,-02.3\%) & 7041 (+10.1\%) \\
      \textbf{nginx} & 170 & 668 & 6155 & 209 (+22.9\%) & 786 (+17.7\%) & 8168 (+32.7\%) \\
      \textbf{node}  & 202 & 843 & 8080 & 165 (\,-18.3\%) & 584 (\,-30.7\%) & 6480 (\,-19.8\%) \\
      \textbf{redis} & 161 & 645 & 6232 & 161 (+00.0\%) & 590 (\,-08.5\%) & 6187 (\,-00.7\%) \\
      \textbf{traefik} & 72 & 194 & 2993 & 52 (\,-27.8\%) & 142 (\,-26.8\%) & 2335 (\,-22.0\%) \\
      \textbf{ubuntu} & \multicolumn{3}{c}{n/a} & 61 (\hspace*{2.2mm}n/a\hspace*{2.2mm}) & 191 (\hspace*{2.2mm}n/a\hspace*{2.2mm}) & 2553 (\hspace*{2.2mm}n/a\hspace*{2.2mm}) \\
      \bottomrule
    \end{tabular}
    \caption{Modusfile and Dockerfile templates only.}
    \label{tab:docker-hub-eval-code-size-lb}
  \end{subtable}
  \begin{subtable}{\columnwidth}
    \centering%
    \begin{tabular}{lrrrrrr}
      \toprule
      {} & \multicolumn{3}{c}{Templating} & \multicolumn{3}{c}{Modus} \\
      \cmidrule(lr){2-4} \cmidrule(lr){5-7}
      \textbf{Project} & \textbf{CR} & \textbf{Words} & \textbf{Bytes} & \textbf{CR} & \textbf{Words} & \textbf{Bytes} \\
      \midrule
      \textbf{mysql} & 318 & 1081 & 11022 & 316 (-00.6\%) & 1012 (-06.4\%) & 11357 (+03.0\%) \\
      \textbf{nginx} & 292 & 907 & 9069 & 209 (-28.4\%) & 786 (-13.3\%) & 8168 (\,-09.9\%) \\
      \textbf{node} & 664 & 2134 & 19122 & 569 (-14.3\%) & 1683 (-21.1\%) & 15414 (\,-19.4\%) \\
      \textbf{redis} & 244 & 952 & 8577 & 207 (-15.2\%) & 773 (-18.8\%) & 7657 (\,-10.7\%) \\
      \textbf{traefik} & 105 & 302 & 3938 & 52 (-50.5\%) & 142 (-53.0\%) & 2335 (\,-40.7\%) \\
      \textbf{ubuntu} & 78 & 254 & 2533 & 69 (-11.5\%) & 212 (-16.5\%) & 2688 (+06.1\%) \\
      \bottomrule
    \end{tabular}
    \caption{Including version fetching (for both) and templating scripts.}
    \label{tab:docker-hub-eval-code-size-ub}
  \end{subtable}

  \caption{Normalised code size for our corpus (\Cref{table:corpus}). The \textbf{CR} column represents line count.}
  \label{tab:docker-hub-eval-code-size}

\end{table*}

\section{Experiment setup}
\label{sec:experiment-setup}

All builds in this section are performed on AWS \textbf{c5.2xlarge} VM, which, at the time of writing, had 8 CPUs, 16 GiB RAM, 10 Gbps bandwidth, and an SSD disk with 8000 IOPS. Some projects build images for platforms other than x86\_64, but we only ran our tests on a x86\_64 VM, not for other architectures. Despite this, our Modusfiles include code to build other architectures, when a project in our corpus does, to ensure a fair code size comparison.

For all of the above projects, our testing process basically consists of the following steps:

\begin{enumerate}
  \item Running the corpus's \texttt{update.sh} (or equivalent), which is used to generate a list of versions and configs, as well as templating their Dockerfiles. The fetched list is also used to generate a Modusfile, in which each entry of the list is represented by a predicate definition like

  \begin{lstlisting}[aboveskip=4pt, belowskip=4pt]
  node_version("12.22.10", "alpine3.14", "amd64", "1.22.17").
  \end{lstlisting}

  All other parts of our Modus input are translated by us from the corpus' Dockerfiles.

  \item Clearing all existing build cache, then pulling all the required base images if they do not exist locally yet.

  \item Running \texttt{modus build} on our Modusfile.

  \item Building all of the Dockerfiles in the current project.\label{item:building-corpus-dockerfile}
\end{enumerate}

Modus v0.1.11 is used for the experiments in this chapter. In step~\ref{item:building-corpus-dockerfile}, all the Dockerfiles are built in parallel
via the GNU Parallel~\cite{tange2011gnuparallel} utility, in order to get a fair comparison. In our case, this will cause a maximum of 8 concurrent \texttt{docker build}. All builds are performed with the BuildKit backend of \lstinline{docker build} by setting the \lstinline{DOCKER_BUILDKIT=1} environment variable.

While the build cache is cleared before builds, downloaded base images like \texttt{alpine} or \texttt{debian} are explicitly not removed between builds, because (1) Docker Hub request limits meant that we could not do a fresh pull for each run; and 2) the time taken to pull images depends on network conditions and CPU speed (for extraction), two problems Modus does not address. For this reason, the time taken to pull base images is not included in any measurements.

\section{Modus Build Time}
\label{sec:docker-hub-eval-result}

\begin{table*}[t]
  \begin{subtable}{\columnwidth}
    \centering%
    \begin{tabular}{lrcrc}
      \toprule
      \textbf{Project} & \multicolumn{2}{c}{\textbf{Dockerfiles (s)}} & \multicolumn{2}{c}{\textbf{Modus (s)}} \\
      \cmidrule(lr){2-3} \cmidrule(lr){4-5}
      & $\mu$ & CI & $\mu$ & CI \\
      \midrule
      \textbf{mysql} & 66.57 & 66.02--67.12 & 68.34 & 68.10--68.57 \\
      \textbf{nginx} & 23.08 & 23.04--23.12 & 26.45 & 26.40--26.51 \\
      \textbf{node} & 108.95 & 108.51--109.39 & 83.30 & 82.26--84.33 \\
      \textbf{redis} & 199.85 & 199.77--199.94 & 202.19 & 199.77--204.61 \\
      \textbf{traefik} & 9.02 & 8.73--9.32 & 8.91 & 8.43--9.38 \\
      \textbf{ubuntu} & 7.86 & 7.81--7.91 & 12.25 & 12.16--12.34 \\
      \bottomrule
    \end{tabular}
    \caption{\texttt{modus build} and parallel \texttt{docker build} only.}
    \label{table:docker-hub-eval-build-time-lb}
  \end{subtable}
  \begin{subtable}{\columnwidth}
    \centering%
    \begin{tabular}{lrcrc}
      \toprule
      \textbf{Project} & \multicolumn{2}{c}{\textbf{Dockerfiles (s)}} & \multicolumn{2}{c}{\textbf{Modus (s)}} \\
      \cmidrule(lr){2-3} \cmidrule(lr){4-5}
      & $\mu$ & CI & $\mu$ & CI \\
      \midrule
      \textbf{mysql} & 69.52 & 68.96--70.07 & 69.67 & 69.42--69.92 \\
      \textbf{nginx} & 23.15 & 23.11--23.18 & 26.45 & 26.40--26.51 \\
      \textbf{node} & 109.99 & 109.55--110.44 & 88.20 & 87.14--89.25 \\
      \textbf{redis} & 200.87 & 200.71--201.04 & 203.35 & 200.92--205.78 \\
      \textbf{traefik} & 9.06 & 8.76--9.35 & 8.91 & 8.43--9.38 \\
      \textbf{ubuntu} & 11.23 & 11.12--11.34 & 12.28 & 12.19--12.37 \\
      \bottomrule
    \end{tabular}
    \caption{Including version fetching (for both) and templating scripts.}
    \label{table:docker-hub-eval-build-time-ub}
  \end{subtable}
  \caption{Build time for each project in our corpus (\Cref{table:corpus}).}
  \label{table:docker-hub-eval-build-time}
\end{table*}

We now compare the corpus' build systems to their Modus ports. These experiments are performed according to the setup described in \Cref{sec:experiment-setup}. For this evaluation, the time spent in the redundant export step discussed in \Cref{sec:handling-multiple-output-images} is excluded from the results, since it is a workaround for BuildKit API limitations and also can not be parallelized together with other build steps effectively. Exporting all output images averages \SI{4.78}{s} across our corpus. When BuildKit exposes an appropriate API, this overhead will disappear.

\Cref{table:docker-hub-eval-build-time} shows our results. Each of the $\mu$ column is an average of 64 runs. The CI columns is the 95\% confidence interval for the true average.

\begin{framed}
	\noindent Modus increases average total build times by \dockerHubTotalTimeAvgDelta, showing that Modus reduces build code size with negligible overhead.
\end{framed}

Modus' implementation is not yet optimised, but still it achieves this
negligible overhead.  Even including the low and input-independent overhead of
the BuildKit workaround (\Cref{sec:handling-multiple-output-images}), developers can safely adopt
Modus today to take advantage of its concision.

\section{Validating Modus-Built Images}

Since our Modusfiles are direct translation of the corpus' existing build systems, we expect Modus images to closely match the Dockerfile images in size and space efficiency, as measured by \texttt{dive}~\cite{diveURL}. Our results show that, on average, our image efficiency score is 0.15\% higher than the Dockerfile counterpart. In the worst case, our efficiency score is not more than 0.05\% lower. The average difference in image size is \SI{1.01}{MiB} (0.4\% of the original size) and the maximum difference is \SI{3.96}{MiB} (3.6\% of the size of that particular image). We also ran simple smoke tests that runs the entry executable with flags to print its version on each image produced: all Modus images passed this test.

%</>
