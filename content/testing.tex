\chapter{Testing}
\label{chap:testing}

Modus is a project with a substantial amount of code which handles a variety of different areas, including logical resolution, transformation between user input, internal representations and BuildKit input, logic for builtin predicates and operators, interfacing with Docker and BuildKit, and a number of other miscellaneous matters. As a programming language, Modus accepts a large space of possible inputs, which can make it difficult to ensure that Modus behaves correctly in all situations. On the other hand, error in Modus has a large impact on the project, as it may cast doubt upon the reliability of images built by Modus.

For this reason, it is important that Modus is tested properly. This short chapter describes various techniques used to test Modus during development. \Cref{sec:future-works} in \cref{chap:conclusions} (Conclusions) also outlines some more sophisticated testing options that this project was not able to explore.

\section{Unit Tests}

Unit tests are small, internal functions which asserts that a certain part of the code behaves as expected~\cite{jan2016innovative}. Rust provides a convenient and standard way to write unit tests: functions can be marked as a unit test with the \lstinline{#[test]} attribute. These functions will be called automatically when a developer runs \lstinline{cargo test} in the command line. Assertions are made using macros which aborts the program when the conditions are not met, causing the test to fail. For example, \lstinline{assert_eq!(a, b)} checks that \lstinline{a == b}.

Following common practices in Rust, these tests are written directly alongside the code they are testing. For example, the file \lstinline{modus-lib/src/builtin.rs} contains both the definition for various builtins, as well as unit tests which construct literals of these predicates and test that they are all resolved correctly.

\section{System Tests}

System tests are written to test that a program executes correctly as a whole, from start to finish~\cite{jan2016innovative}. In Modus, system tests are done by building Modusfiles and asserting that the set of output images fulfil certain conditions, such as having a certain number of outputs, containing a file at a particular place with some specific content, or having certain image properties.

We decided to write system tests for Modus in the form of a collection of Python scripts. They can be found in the \lstinline{tests} directory. Those scripts contain functions that each test a particular aspect of Modus, such as a specific builtin predicate, or the proof solver as a whole. A simple example is shown in \Cref{lst:example-system-test}.

{%
\newcommand\pr[1]{%
  \href{https://github.com/modus-continens/modus/pull/#1}{\##1}%
  % \footnote{\url{https://github.com/modus-continens/modus/pull/#1}}%
}%
78 separates tests were written throughout the development process, each testing one particular Modusfile. Together with unit tests, they act as convincing evidence for the correctness of Modus source code at any given point. They improve development efficiency, as developers do not have to worry about manually checking all edge cases when developing or changing features, and also helps catch bugs or design oversights early, such as in the implementation of \lstinline{::merge} (pull request \pr{128}, \pr{139}), or when implementing complex query parsing (\pr{124}).
% or when required updates to related files were overlooked in \pr{142}.
}

\begin{listing}[t]
  \centering%
  \begin{minipage}{27em}
    \begin{minted}{python}
      def test_1(self):
        mf = dedent("""\
          a :- from("alpine").
          b :- a, run("echo aaa > /tmp/a").""")
        imgs = self.build(mf, "b")
        self.assertEqual(len(imgs), 1)
        first_img = imgs[Fact("b", ())]
        self.assertEqual(first_img.read_file("/tmp/a"), "aaa\n")

        imgs = self.build(mf, "a")
        self.assertEqual(len(imgs), 1)
        first_img = imgs[Fact("a", ())]
        self.assertFalse(first_img.contains_file("/tmp/a"))
    \end{minted}
  \end{minipage}

  \caption{An example system test in \lstinline{test_simple.py}}
  \label{lst:example-system-test}
\end{listing}

\section{Continuous Integration}

Continuous Integration (CI) is the practice of building and testing an application in an automated manner each time a change is made, typically on a build server~\cite{ci-microsoft}. This allows errors (also commonly known as \emph{regressions}) to be caught early, enables developers to merge in their code more frequently and with more confidence, and can also simplify the release process by automating tasks.

Modus' source code is hosted on GitHub, which comes with a built-in CI platform known as ``GitHub Actions''~\cite{gh-actions} that integrates well with other GitHub features such as pull requests. A workflow has been created which will automatically run all unit and system tests each time a pull request is made. This workflow also builds the image for the BuildKit frontend and push it to GitHub container registry, as discussed in \Cref{sec:invoking-buildkit}.

The CI workflow can also be used to simplify the process of releasing a new version, by performing basic checks (such as consistent versioning), building binaries for all supported platforms, and pushing them to a draft GitHub release, which can be released to the public in one click. The code for this workflow can be found in \lstinline{.github/workflows/rust.yml}.
