.PHONY: clean main.pdf standalone-abstract.pdf all

all: main.pdf standalone-abstract.pdf

main.pdf:
	pdflatex -shell-escape main.tex
	biber main
	pdflatex -shell-escape main.tex

standalone-abstract.pdf:
	pdflatex -shell-escape standalone-abstract.tex
	pdflatex -shell-escape standalone-abstract.tex

clean:
	git clean -fX
